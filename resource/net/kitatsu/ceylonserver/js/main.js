var menuClose = true;
function toggleMenu() {
  //.menu-buttonが押される度にメニューの開閉を行う
  if (menuClose) {
    $('#navul > li').css('display', 'block');
  } else {
    $('#navul > li').css('display', 'none');
  }
  menuClose = !menuClose;
}

function resetMenu() {
  menuClose = true;
  $('#navul > li').css('display', '');
}

$(window).resize(function() {
  //ウインドウのリサイズ時にメニューのリセットを行っておく
  resetMenu();
});

$(function(){
  //ページロード完了時
  $('.menu-button').on('click', toggleMenu);
});
