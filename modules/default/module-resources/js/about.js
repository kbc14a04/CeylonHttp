$(function(){
  var props = new Array();
  props[1] = {
    fontSize: 'small'
  };
  props[2] = {};
  props[3] = {
    fontSize: 'large'
  };
  props[4] = {
    fontSize: 'x-large'
  };
  props[5] = {
    fontSize: 'xx-large'
  };

  for (var i = 1; i <= 5; i++) {
    $('[data-importance=' + i + ']').css(props[i]);
  }
});
