import ceylon.html {
	H2,
	Ul,
	Li,
	P
}
class Link() extends Page(){

	pageName = "link";
	description = "";

	contents = [

	H2("外部リンク"),

	Ul {
		Li {
			P("まだリンクが設定されていません。")
		}
	}

	];

}