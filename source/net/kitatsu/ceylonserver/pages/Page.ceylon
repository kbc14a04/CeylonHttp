import ceylon.html {
	Html,
	BlockOrInline,
	Snippet
}

"各ページをまとめて扱うための抽象クラスです。"
abstract class Page() {

	"ページ名"
	shared formal String pageName;

	"metaタグのdescriptionに記載する文章"
	shared formal String description;

	"ページ固有CSSファイルを持っているか"
	shared default Boolean pageCss = false;

	"ページ固有JSファイルを持っているか"
	shared default Boolean pageJs = false;

	"このページのコンテンツ"
	shared formal {<BlockOrInline|{BlockOrInline*}|Snippet<BlockOrInline>|Null>*} contents;

	"このページのHTMLオブジェクト"
	see(`class Template`)
	shared Html html => Template(contents, pageName, description, pageCss, pageJs).html;

}