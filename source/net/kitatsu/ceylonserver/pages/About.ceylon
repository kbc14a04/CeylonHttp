import ceylon.html {
	H2,
	H3,
	P,
	Ul,
	Li
}
import net.kitatsu.ceylonserver {
	handleName,
	homeURI
}

"サイトポリシとかそのへん"
class About() extends Page(){

	pageName = "about";
	pageCss = true;
	pageJs = true;
	description = "``homeURI``と、管理者についての説明です。";

	contents = [
	H2("このサイトについて"),
	P ("``handleName``が個人的に作成したプログラムを置いたり、<br />
	    諸々のお知らせをしたりするかもしれない程度のスペースです。"),
	H3("自己紹介"),
	P ("プログラミングとかします。現在専門学校にて諸々勉強中"),
	H3("さわったもの、興味のあるキーワード"),
	Ul {
		id = "interests";
		Li{text="Ceylon"; data=["importance"->5];},
		Li{text="Java"; data=["importance"->4];},
		Li{text="C# .NET"; data=["importance"->2];},
		Li{text="PHP"; data=["importance"->3];},
		Li{text="JavaScript"; data=["importance"->5];},
		Li{text="HTML5"; data=["importance"->1];},
		Li{text="CSS3";data=["importance"->3];},
		Li{text="Sass(Scss) + Compass";data=["importance"->4];},
		Li{text="Linux";data=["importance"->2];},
		Li{text="SQL";data=["importance"->3];},
		Li{text="Android";data=["importance"->2];},
		Li{text="セキュアプログラミング";data=["importance"->1];},
		Li{text="自作PC";data=["importance"->2];}
	}
	];

}