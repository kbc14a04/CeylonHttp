import ceylon.html {
	Section,
	A,
	P,
	Br,
	Img,
	H2,
	H3
}
import ceylon.time {
	date
}
import net.kitatsu.ceylonserver { imgDirURI }

class Prog() extends Page(){

	pageName = "prog";
	pageCss = true;
	description = "制作したプログラムの紹介ページです。";

	contents = [

	H2("つくったプログラム"),

	Section {
		classNames = "prog-sec";
		H3("このWebサイト"),
		Img(imgDirURI + "ceylonserver.png", "動作例の画像"),Br(),
		A("説明記事", "#"),
		P {
			text = "<time>" + date(2015,9,10).string + "</time>";
			classNames = "date";
		},
		P("JVM上で動作するCeylonプログラムを主として構築しています。<br />Scss+Compass, JavaScriptもちょっとだけ利用しています。")
	},

	Section {
		classNames = "prog-sec";
		H3("せんしゃげー"),
		Img(imgDirURI + "tangame.png", "せんしゃげーの画像"),Br(),
		A("説明ページ", "#"),
		P {
			text = "<time>" + date(2015,9,10).string + "</time>";
			classNames = "date";
		},
		P("Java(Swing)で開発していたゲームです。")
	}

	];

}