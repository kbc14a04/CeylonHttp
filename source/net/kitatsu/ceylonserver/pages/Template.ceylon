import ceylon.html {
	Html,
	html5,
	Footer,
	Body,
	H1,
	Header,
	Head,
	Section,
	CharsetMeta,
	Link,
	stylesheet,
	css,
	Script,
	javascript,
	Nav,
	Ul,
	Li,
	Button,
	button,
	BlockOrInline,
	Snippet,
	P,
	Meta
}
import ceylon.collection {
	ArrayList
}
import net.kitatsu.ceylonserver {
	siteName,
	menuItems,
	jsDirURI,
	jQueryCDNURI,
	cssDirURI,
	handleName
}

"ページの枠組み部分の生成を担当します。"
see(`class Page`)
shared class Template(mainContents, pageName, description, pageCss, pageJs) {

	"ページ固有要素としてdiv.mainに挿入するコンテンツ"
	{<BlockOrInline|{BlockOrInline*}|Snippet<BlockOrInline>|Null>*} mainContents;

	"ページ名を表す文字列。
	 - /css/pageName.css
	 - /js/pageName.js
	 に対してHTTPリクエストを飛ばす。"
	String pageName;

	"ページの概要"
	String description;

	//Settingsのメニューアイテムをもとにして、ナビゲーションに使うLiの配列を生成
	value menuLis = ArrayList<Li>();
	for (a in menuItems) {
		menuLis.add(Li{a});
	}

	"ページ固有のCSSファイルを持つか"
	Boolean pageCss;

	"ページ固有のJSファイルを持つか"
	Boolean pageJs;

	//ページ固有のCSSおよびJSファイルへのリンクの生成
	value headChilds = ArrayList<Link|Script>();
	if (pageCss) {
		headChilds.add(Link(stylesheet, cssDirURI + pageName + ".css"));
	}
	if (pageJs) {
		headChilds.add(Script(jsDirURI + pageName + ".js", javascript));
	}

	String title() {
		value f = pageName.first;
		if (exists f) {
			value uf = f.uppercased;
			return siteName + " : " + uf.string + pageName.rest;
		}
		return siteName;
	}


	"テンプレートから生成されたHtml要素"
	shared Html html = Html{

		doctype = html5;

		head = Head {
			title = title();
			CharsetMeta("utf-8"),
			Meta("description", description),
			Link { //汎用css参照link
				rel = stylesheet;
				type = css;
				href= cssDirURI + "main.css";
			},
			Script { //jQuery用Script
				src = jQueryCDNURI;
				type = javascript;
			},
			Script { //汎用js参照link
				src = jsDirURI + "main.js";
				type = javascript;
			},
			*headChilds //ページ固有cssおよびjsが有効であれば付加
		};

		body = Body {
			lang = "ja";

			Header {
				H1(siteName),
				Nav { //ヘッダーのナビゲーション
					Button{ //メニュー開閉用ボタン　/js/main.js内でイベントのハンドリングをしている
						type = button;
						text = "Menu";
						classNames = "menu-button";
					},
					Ul { //メニューアイテム
						id = "navul";
						menuLis
					}
				}
			},

			Section {
				classNames = "main";
				children = mainContents;	//引数で受け取ったhtml要素をここに配置
			},

			Footer {
				P("Copyright &copy; 2015 ``handleName``")
			}
		};
	};

}