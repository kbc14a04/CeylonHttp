import ceylon.html {
	H2, P, Br
}

"404ページを提供する。"
class NotFound() extends Page(){

	pageName = "not_found";
	description= "要求されたページが存在しない場合に表示されます。";

	contents = [

	H2("Not Found"),
	P("ページが見つかりませんでした。"), Br(),
	P("URLが間違っているか、すでに削除されたページかもしれません。")

	];

}