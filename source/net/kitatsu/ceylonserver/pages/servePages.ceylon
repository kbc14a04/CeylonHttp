import ceylon.net.http.server {
	Request,
	Response
}
import ceylon.net.http {
	Header
}

"HTMLコンテンツを提供する。"
shared void servePages(Request request, Response response) {

	//HTTPヘッダにMIMEタイプとエンコーディング情報を不可
	response.addHeader(Header("Content-Type", "text/html; charset=utf-8"));

	//今回のリクエストで提供するページ
	Page() page;

	//ゲットパラメタ「p」の値を取得
	value p = request.parameter("p");

	if (exists p) {
		switch (p)
		case ("about") {
			page = About;
		}
		case ("link") {
			page = Link;
		}
		case ("prog") {
			page = Prog;
		}
		case ("news") {
			page = News;
		}
		else {
			//パラメタの値が不正であるとき
			page = NotFound;
		}
	} else {
		//パラメタpがなかった時
		page = Home;
	}

	//HTMLを生成して返答
	response.writeString(page().html.string);

}