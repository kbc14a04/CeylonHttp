import ceylon.html {
	H2,
	Span,
	A,
	Ul,
	Li,
	Snippet
}
import ceylon.time {
	Date,
	date
}

"お知らせリストです。"
Ul newsList = Ul {
	NewsItem("開設しました。", date(2015,9,10))
};

"ニュースリストに使用するLi要素を生成して返します。"
class NewsItem(String newsText, Date day, String? link = null) satisfies Snippet<Li>{
	content = (link is Null)
	then Li {
		Span {
			classNames = "news-text";
			text = newsText;
		},
		Span {
			classNames = "news-date";
			text = day.string;
		}
	}
	else Li {
		A {
			classNames = "news-link";
			text = newsText;
			href = link else "";
		},
		Span {
			classNames = "news-date";
			text = day.string;
		}
	};
}

"過去のお知らせ一覧を表示するページです。"
class News() extends Page() {

	pageName = "news";

	description = "過去のお知らせ一覧を表示するページです。";

	contents = [
	H2("過去のお知らせ一覧"),
	newsList
	];

}