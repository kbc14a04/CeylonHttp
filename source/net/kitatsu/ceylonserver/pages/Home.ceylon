import ceylon.html {
	Div,
	Li,
	Snippet,
	Ul,
	A,
	H2,
	H3,
	P
}
import ceylon.collection {

	ArrayList
}

"ページトップ用"
class Home() extends Page(){

	pageName = "home";

	description = "トップページです。";

	pageCss = true;

	value topNews = ArrayList<Li|{Li*}|Snippet<Li>|Null>();
	for (idx->li in newsList.children.indexed) {
		if (idx >= 5) {
			break;
		}
		topNews.add(li);
	}
	topNews.add(
		Li{
			id="more-news";
			A {
				href = "/?p=news";
				text = "履歴";
			}
		}
	);

	contents = [
		H2 {
			id = "home-hedding";
			text = "サイトホーム";
		},
		Div {
			id = "top-news";
			H3("news:"),
			Ul {
				*topNews
			}
		},
		P {
			id = "module-ver";
			text = `module net.kitatsu.ceylonserver`.string;
		}
	];

}