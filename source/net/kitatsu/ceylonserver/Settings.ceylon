import ceylon.html { A }

"サイト名"
shared String siteName = "Web Site";

"管理者名"
shared String handleName = "KITATSU";

"サイトホームのURL"
shared String homeURI = "http://localhost:8889/";

"css用ディレクトリのURI"
shared String cssDirURI = homeURI + "css/";

"js用ディレクトリのURI"
shared String jsDirURI = homeURI + "js/";

"画像ファイル用のURI"
shared String imgDirURI = homeURI + "img/";

"jQueryのCDNに使うURI"
shared String jQueryCDNURI = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";

"ページトップのヘッダー内ナビゲーションに表示するリンク"
shared A[] menuItems = [
	A("Home",	"/"									),
	A("About",	"/?p=about"							),
	A("Prog",	"/?p=prog"							),
	A("Blog",	"http://tsuyoshi-kita.tumblr.com/"	),
	A("Link",	"/?p=link"							)
];