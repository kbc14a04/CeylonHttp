import ceylon.io {
	SocketAddress
}
import ceylon.net.http {
	get,
	post
}
import ceylon.net.http.server {
	Endpoint,
	AsynchronousEndpoint,
	newServer,
	endsWith,
	isRoot
}
import net.kitatsu.ceylonserver.pages {
	servePages
}
import net.kitatsu.ceylonserver.resources {
	serveResources
}

"Run the module `ceylonserver`."
shared void run() {

    value server = newServer {

        AsynchronousEndpoint {
            //js,cssファイルのリクエスト時
            path = endsWith(".js").or(endsWith(".css"));
            service = serveResources;
            acceptMethod = [get];
        },
		Endpoint {
			//ページリクエストに答える
			path = isRoot();
			service = servePages;
			acceptMethod = [get, post];
		}

    };

	//サーバーの起動
    server.start(SocketAddress("localhost", 8889));

}