import ceylon.net.http.server {
	Request,
	Response
}
import ceylon.net.http {
	Header
}

shared void serveResources (Request request, Response response, void complete()) {

	value mod = `module net.kitatsu.ceylonserver`;

	value sppath = request.path.split('/'.equals);

	value dir = sppath.getFromFirst(1) else "no-dir";
	value name = sppath.getFromFirst(2) else "no-name";

	switch (dir)
	case ("js" | "css") {
		response.addHeader(Header("Content-Type", "text/``dir``; charset=utf-8"));
		value rs = mod.resourceByPath("``dir``/``name``");
		if (exists rs) {
			response.writeString(rs.textContent());
		} else {
			//ファイルが存在しない場合
		}
	}
	case ("img") {

	}
	else {

	}

	complete();

}